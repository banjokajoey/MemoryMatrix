-----------------------------------------------------------------------------------------
--
-- grid.lua
--
-----------------------------------------------------------------------------------------

local grid = {}


--------------------------------------------
-- Imports
--------------------------------------------
local colors = require "colors"
local fonts  = require "fonts"
local level  = require "level"
local ui     = require "ui"


--------------------------------------------
-- Constants
--------------------------------------------
local GRID_PADDING = ui.X_PADDING
local CELL_PADDING = 0.5 * ui.X_PADDING


--------------------------------------------
-- Public methods
--------------------------------------------
grid.drawGrid = function(gridDim, screenDim)
    -- Bounding box
    local boundingBox = display.newRoundedRect(0, 0, screenDim, screenDim, ui.CORNER_RAD)
    
    -- Cells
    local cellDim = ((screenDim - 2 * GRID_PADDING - ((gridDim - 1) * CELL_PADDING)) / gridDim)
    local cells = {}
    local cellGroups = {}
    local minX = -0.5 * screenDim
    local minY = -0.5 * screenDim
    for row = 1, gridDim do
        for col = 1, gridDim do
            local cellGroup = display.newGroup()
            local cell = display.newRoundedRect(0, 0, cellDim, cellDim, ui.CORNER_RAD)
            cellGroup:insert(cell)
            cellGroup.x = minX + GRID_PADDING + ((col - 1) + 0.5) * (cellDim + CELL_PADDING)
            cellGroup.y = minY + GRID_PADDING + ((row - 1) + 0.5) * (cellDim + CELL_PADDING)
            colors.colorObject(cell, colors.BLACK)
            table.insert(cells, cell)
            table.insert(cellGroups, cellGroup)
        end
    end

    -- Display group
    local gridGroup = display.newGroup()
    gridGroup:insert(boundingBox)
    for i = 1, #cellGroups do
        gridGroup:insert(cellGroups[i])
    end

    -- Object
    gridObject = {
        dimensions = gridDim,
        cells = cells,
        cellGroups = cellGroups,
        cellDimension = cellDim
    }

    -- Return
    return gridGroup, gridObject
end


grid.renderLevel = function(gridObject, levelObject)
    local cells = gridObject.cells
    for i = 1, #cells do
        local cell = cells[i]
        local value = levelObject.grid[i]
        if value then
            colors.colorObject(cell, colors.SKY_BLUE)
        else
            colors.colorObject(cell, colors.BLACK)
        end
    end
end


grid.clearLevel = function(gridObject)
    local cells = gridObject.cells
    for i = 1, #cells do
        colors.colorObject(cells[i], colors.BLACK)
    end
end


grid.numberCells = function(gridObject)
    local cellGroups = gridObject.cellGroups
    for i = 1, #cellGroups do
        local cellGroup = cellGroups[i]
        local numberText = display.newText({
            text = "" .. i,
            font = fonts.getFontBold(),
            fontSize = 0.33 * gridObject.cellDimension
        })
        cellGroup:insert(numberText)
    end
end


--------------------------------------------
-- Return
--------------------------------------------
return grid