-----------------------------------------------------------------------------------------
--
-- background_generator.lua
--
-----------------------------------------------------------------------------------------

local background_generator = {}


--------------------------------------------
-- Imports
--------------------------------------------
local colors = require "colors"


--------------------------------------------
-- Public methods
--------------------------------------------
background_generator.getBackgroundDisplay = function()
    local bgRect = display.newRect(0, 0, display.contentWidth, display.contentHeight)
    bgRect.anchorX = 0.0
    bgRect.anchorY = 0.0
    colors.colorObject(bgRect, colors.DARKEST_BLUE) --colors.DARK_GRAY)

    local bgDisplay = display.newGroup()
    bgDisplay:insert(bgRect)
    return bgDisplay
end


background_generator.getSplashBackgroundDisplay = function()
    local bgRect = display.newRect(0, 0, display.contentWidth, display.contentHeight)
    bgRect.anchorX = 0.0
    bgRect.anchorY = 0.0
    colors.colorObject(bgRect, colors.BLACK)

    local bgDisplay = display.newGroup()
    bgDisplay:insert(bgRect)
    return bgDisplay
end


--------------------------------------------
-- Return
--------------------------------------------
return background_generator