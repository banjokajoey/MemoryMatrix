-----------------------------------------------------------------------------------------
--
-- fonts.lua
--
-----------------------------------------------------------------------------------------

local fonts = {}


-------------------------------------
-- Script variables
-------------------------------------
local plainFont = native.systemFont
local boldFont = native.systemFontBold

-------------------------------------
-- Helper methods
-------------------------------------
local function cacheFontNames()
    local helveticaSearchStr = "arial" --"helvetica"
    local boldSearchStr = "bold"

    for i, fontName in ipairs(native.getFontNames()) do
        local sanitizedFontName = string.lower(fontName)
        if string.find(sanitizedFontName, helveticaSearchStr) then
            if sanitizedFontName == helveticaSearchStr then
                plainFont = fontName
            elseif string.find(sanitizedFontName, boldSearchStr) then
                if #sanitizedFontName <= #helveticaSearchStr + #boldSearchStr + 1 then
                    boldFont = fontName
                end
            end
        end
    end
end


-------------------------------------
-- Public methods
-------------------------------------
fonts.getFont = function()
    return plainFont
end


fonts.getFontBold = function()
    return boldFont
end


--------------------------------------------
-- Initialize script state
--------------------------------------------
cacheFontNames()


--------------------------------------------
-- Return
--------------------------------------------
return fonts