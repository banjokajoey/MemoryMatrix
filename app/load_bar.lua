-----------------------------------------------------------------------------------------
--
-- load_bar.lua
--
-----------------------------------------------------------------------------------------

local loadBar = {}


--------------------------------------------
-- Imports
--------------------------------------------
local colors = require "colors"
local ui     = require "ui"


--------------------------------------------
-- Public methods
--------------------------------------------
loadBar.drawLoadBar = function(barWidth, barHeight)
    local loadBar = display.newRect(-0.5 * barWidth, 0, barWidth, barHeight)
    loadBar.anchorX = 0.0
    colors.colorObject(loadBar, colors.LIGHT_GREEN)

    local bgBar = display.newRect(0, 0, barWidth, barHeight)
    colors.colorObject(bgBar, colors.BLACK)

    local brainImage = display.newImage('images/brain.png')
    brainImage.width = 4 * barHeight
    brainImage.height = 4 * barHeight

    local barGroup = display.newGroup()
    barGroup:insert(bgBar)
    barGroup:insert(loadBar)
    barGroup:insert(brainImage)

    local barObject = {
        loadBar = loadBar,
        fullWidth = barWidth,
        brainImage = brainImage
    }

    return barGroup, barObject
end


loadBar.emptyBar = function(barObject, timeToEmpty, onComplete)
    local loadBarRect = barObject.loadBar
    transition.to(loadBarRect, {
        width = 0.0,
        time = timeToEmpty,
        onComplete = onComplete
    })

    local brainImage = barObject.brainImage
    brainImage.x = loadBarRect.x + loadBarRect.width
    transition.to(brainImage, {
        x = loadBarRect.x,
        time = timeToEmpty
    })
end


loadBar.fillBar = function(barObject, timeToFill, onComplete)
    local loadBarRect = barObject.loadBar
    transition.to(loadBarRect, {
        width = barObject.fullWidth,
        time = timeToFill,
        onComplete = onComplete
    })

    local brainImage = barObject.brainImage
    brainImage.x = loadBarRect.x + loadBarRect.width
    transition.to(brainImage, {
        x = loadBarRect.x + barObject.fullWidth,
        time = timeToFill
    })
end


loadBar.setBarRatio = function(barObject, fillRatio)
    barObject.loadBar.width = fillRatio * barObject.fullWidth
    local brainImage = barObject.brainImage
    brainImage.x = barObject.loadBar.x + barObject.loadBar.width
end


loadBar.setBarColor = function(barObject, newColor)
    colors.colorObject(barObject.loadBar, newColor)
end


--------------------------------------------
-- Return
--------------------------------------------
return loadBar