-----------------------------------------------------------------------------------------
--
-- play.lua
--
-----------------------------------------------------------------------------------------

--------------------------------------------
-- Imports
--------------------------------------------
local bg_gen       = require "background_generator"
local buttons      = require "buttons"
local colors       = require "colors"
local composer     = require "composer"
local fonts        = require "fonts"
local grid_lib     = require "grid"
local level_lib    = require "level"
local load_bar_lib = require "load_bar"
local ui           = require "ui"


--------------------------------------------
-- Script Constants
--------------------------------------------
-- Game title
local TITLE_Y         = 0.125 * display.contentHeight
local TITLE_FONTSIZE  = 0.0625 * display.contentHeight 
local TITLE_MAX_WIDTH = 0.925 * display.contentWidth

-- Level dimensions
local GRID_DIM = 6
local LEVEL_WIDTH = ui.LOAD_BAR_WIDTH

-- Instructions text
local INSTRUCTIONS_Y = 0.95 * display.contentHeight
local INSTRUCTIONS_FONTSIZE = 0.04 * display.contentHeight
local INSTRUCTIONS_MAX_WIDTH = TITLE_MAX_WIDTH

-- Load bar
local LOAD_BAR_Y = 0.875 * display.contentHeight


--------------------------------------------
-- Script Variables
--------------------------------------------
local scene = composer.newScene()
local levelObject = nil
local levelNumber = -1
local gridObject = nil
local loadBarObject = nil
local titleText = nil
local instructionsText = nil
local levelIsSolved = false


--------------------------------------------
-- Level complete mode
--------------------------------------------

function enterLevelCompleteMode()
    levelIsSolved = true
    instructionsText.text = "Well done!"
    timer.performWithDelay(1250, function() 
        composer.gotoScene("scenes.game_over", {
            effect = "slideLeft", 
            time = 500,
            params = {
                levelNumber = levelNumber,
                numCorrect = levelObject.numCorrect,
                numIncorrect = levelObject.numIncorrect,
                predictedDifficulty = math.random(),
                actualDifficulty = math.random()
            }
        })
    end)
end


--------------------------------------------
-- Interactive mode
--------------------------------------------
local function onCellPress(event, cell, cellIndex)
    if event.phase ~= "ended" then
        return
    end

    if levelIsSolved then
        return
    end

    local isCorrect = levelObject.grid[cellIndex]
    if isCorrect then
        colors.colorObject(cell, colors.LIGHT_GREEN)
        levelObject.numCorrect = levelObject.numCorrect + 1
    else
        colors.colorObject(cell, colors.RED)
        levelObject.numIncorrect = levelObject.numIncorrect + 1
    end

    if levelObject.numCorrect == levelObject.numValues then
        enterLevelCompleteMode()
    end
    
    return true
end


function enterInteractiveMode()
    instructionsText.text = "Recreate!"
    grid_lib.clearLevel(gridObject)
    for i = 1, #gridObject.cells do
        local cell = gridObject.cells[i]
        cell:addEventListener("touch", function(event) onCellPress(event, cell, i) end)
    end
end


--------------------------------------------
-- Memorize mode
--------------------------------------------

function enterMemorizeMode()
    levelObject = level_lib.getLevel(levelNumber)
    grid_lib.renderLevel(gridObject, levelObject)
    instructionsText.text = "Memorize."
    load_bar_lib.setBarRatio(loadBarObject, 1.0)
    load_bar_lib.emptyBar(loadBarObject, 3000, enterInteractiveMode)
end


--------------------------------------------
-- Loading mode
--------------------------------------------

function enterLoadingMode()
    instructionsText.text = "Personalizing question..."
    load_bar_lib.fillBar(loadBarObject, 2000, function()
        instructionsText.text = "Ready"
        timer.performWithDelay(1000, function()
            instructionsText.text = "Set"
            timer.performWithDelay(1000, function()
                enterMemorizeMode()
            end)
        end)
    end)
end


--------------------------------------------
-- Create Scene
--------------------------------------------
function scene:create(event)    
    -- Called when the scene's view does not exist
    local sceneGroup = self.view

    -- Add background
    local background = bg_gen.getBackgroundDisplay()
    sceneGroup:insert(background)

    -- Title text
    titleText = display.newText({
        text = "LEVEL X",
        x = display.contentCenterX,
        y = TITLE_Y,
        font = fonts.getFontBold(),
        fontSize = TITLE_FONTSIZE,
        align = "center"
    })
    titleText:setFillColor(1.0)
    ui.constrainTextSizeIfNecessary(titleText, TITLE_MAX_WIDTH)
    sceneGroup:insert(titleText)

    -- Level grid
    local grid, gridObj = grid_lib.drawGrid(GRID_DIM, LEVEL_WIDTH)
    gridObject = gridObj
    grid.x = display.contentCenterX
    grid.y = display.contentCenterY
    sceneGroup:insert(grid)

    -- Level
    levelObject = level_lib.loadBlank()
    grid_lib.renderLevel(gridObject, levelObject)

    -- Instructions
    instructionsText = display.newText({
        text = "",
        x = display.contentCenterX,
        y = INSTRUCTIONS_Y,
        font = fonts.getFontBold(),
        fontSize = INSTRUCTIONS_FONTSIZE,
        align = "center"
    })
    instructionsText:setFillColor(1.0)
    ui.constrainTextSizeIfNecessary(instructionsText, INSTRUCTIONS_MAX_WIDTH)
    sceneGroup:insert(instructionsText)

    -- Load bar
    local loadBar, loadBarObj = load_bar_lib.drawLoadBar(ui.LOAD_BAR_WIDTH, ui.LOAD_BAR_HEIGHT)
    loadBarObject = loadBarObj
    loadBar.x = display.contentCenterX
    loadBar.y = LOAD_BAR_Y
    sceneGroup:insert(loadBar)
    load_bar_lib.setBarRatio(loadBarObject, 0.0)
end


--------------------------------------------
-- Show Scene
--------------------------------------------
function scene:show(event)
    local sceneGroup = self.view
    local phase = event.phase
    local levelParams = event.params
    
    if phase == "will" then
        -- Called when the scene is still off screen and is about to move on screen
        if levelParams ~= nil then
            levelNumber = levelParams.levelNumber
            titleText.text = "DIFFICULTY " .. levelNumber
        end
    elseif phase == "did" then
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc.
        enterLoadingMode()
    end 
end


--------------------------------------------
-- Hide Scene
--------------------------------------------
function scene:hide(event)
    local sceneGroup = self.view
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
    elseif phase == "did" then
        -- Called when the scene is now off screen
        composer.removeScene("scenes.play")
    end 
end


--------------------------------------------
-- Destroy Scene
--------------------------------------------
function scene:destroy(event)
    local sceneGroup = self.view
    
    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
end


--------------------------------------------
-- Scene listener setup
--------------------------------------------
scene:addEventListener("create" , scene)
scene:addEventListener("show"   , scene)
scene:addEventListener("hide"   , scene)
scene:addEventListener("destroy", scene)
return scene