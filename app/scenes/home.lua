-----------------------------------------------------------------------------------------
--
-- home.lua
--
-----------------------------------------------------------------------------------------

--------------------------------------------
-- Imports
--------------------------------------------
local bg_gen       = require "background_generator"
local buttons      = require "buttons"
local colors       = require "colors"
local composer     = require "composer"
local fonts        = require "fonts"
local load_bar_lib = require "load_bar"
local ui           = require "ui"


--------------------------------------------
-- Script Constants
--------------------------------------------
-- Game title
local TITLE_Y         = 0.19 * display.contentHeight
local TITLE_MAX_WIDTH = 0.925 * display.contentWidth
local TITLE_FONTSIZE  = 0.0625 * display.contentHeight 

-- Play button
local PLAY_BTN_DIAMETER = 0.35 * display.contentHeight
local PLAY_BTN_CENTER_Y = 0.535 * display.contentHeight

-- Load bar
local LOAD_BAR_Y = 0.85 * display.contentHeight

-- Loading message
local LOAD_TEXT_Y = 0.925 * display.contentHeight
local LOAD_TEXT_FONTSIZE = 0.04 * display.contentHeight
local LOAD_TEXT_MAX_WIDTH = 0.925 * display.contentWidth


--------------------------------------------
-- Script Variables
--------------------------------------------
local scene = composer.newScene()
local loadBarObject = nil
local loadText = nil
local playButton = nil
local playButtonIsActive = false


--------------------------------------------
-- Button Events
--------------------------------------------
local function onPlayBtnPress(event)
    if event.phase ~= "ended" then
        return
    end
    if not playButtonIsActive then
        return
    end
    composer.gotoScene("scenes.select_difficulty", "slideLeft", 500)
    return true
end


--------------------------------------------
-- Helper functions
--------------------------------------------
local function activatePlayButton()
    transition.to(playButton, {
        alpha = 1.0,
        time = 100
    })
    playButtonIsActive = true
    loadText.text = "School is in session."
end


local function deactivatePlayButton()
    playButton.alpha = 0.5
    playButtonIsActive = false
    loadText.text = "Syncing with Headmaster..."
end


--------------------------------------------
-- Create Scene
--------------------------------------------
function scene:create(event)    
    -- Called when the scene's view does not exist
    local sceneGroup = self.view

    -- Add background
    local background = bg_gen.getBackgroundDisplay()
    sceneGroup:insert(background)

    -- Title text
    local titleText = display.newText({
        text = "MEMORY MATRIX",
        x = display.contentCenterX,
        y = TITLE_Y,
        font = fonts.getFontBold(),
        fontSize = TITLE_FONTSIZE,
        align = "center"
    })
    titleText:setFillColor(1.0)
    ui.constrainTextSizeIfNecessary(titleText, TITLE_MAX_WIDTH)
    sceneGroup:insert(titleText)

    -- Play button
    playButton = buttons.drawIconButton(buttons.PLAY, PLAY_BTN_DIAMETER)
    playButton.x = display.contentCenterX
    playButton.y = PLAY_BTN_CENTER_Y
    sceneGroup:insert(playButton)
    playButton:addEventListener("touch", onPlayBtnPress)

    -- Load bar
    local loadBar, loadBarObj = load_bar_lib.drawLoadBar(ui.LOAD_BAR_WIDTH, ui.LOAD_BAR_HEIGHT)
    loadBarObject = loadBarObj
    load_bar_lib.setBarRatio(loadBarObject, 0.0)
    load_bar_lib.setBarColor(loadBarObject, colors.GOLD)
    loadBar.x = display.contentCenterX
    loadBar.y = LOAD_BAR_Y
    sceneGroup:insert(loadBar)

    -- Load message
    loadText = display.newText({
        text = "Syncing master teacher...",
        x = display.contentCenterX,
        y = LOAD_TEXT_Y,
        font = fonts.getFontBold(),
        fontSize = LOAD_TEXT_FONTSIZE,
        align = "center"
    })
    loadText:setFillColor(1.0)
    ui.constrainTextSizeIfNecessary(loadText, LOAD_TEXT_MAX_WIDTH)
    sceneGroup:insert(loadText)

    -- Set scene
    deactivatePlayButton()
end


--------------------------------------------
-- Show Scene
--------------------------------------------
function scene:show(event)
    local sceneGroup = self.view
    local phase = event.phase
    
    if phase == "will" then
        -- Called when the scene is still off screen and is about to move on screen
    elseif phase == "did" then
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc.
        load_bar_lib.fillBar(loadBarObject, 2500, activatePlayButton)
    end 
end


--------------------------------------------
-- Hide Scene
--------------------------------------------
function scene:hide(event)
    local sceneGroup = self.view
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
    elseif phase == "did" then
        -- Called when the scene is now off screen
        composer.removeScene("scenes.home")
    end 
end


--------------------------------------------
-- Destroy Scene
--------------------------------------------
function scene:destroy(event)
    local sceneGroup = self.view
    
    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
end


--------------------------------------------
-- Scene listener setup
--------------------------------------------
scene:addEventListener("create" , scene)
scene:addEventListener("show"   , scene)
scene:addEventListener("hide"   , scene)
scene:addEventListener("destroy", scene)
return scene