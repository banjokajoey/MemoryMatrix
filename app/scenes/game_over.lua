-----------------------------------------------------------------------------------------
--
-- game_over.lua
--
-----------------------------------------------------------------------------------------

--------------------------------------------
-- Imports
--------------------------------------------
local bg_gen       = require "background_generator"
local buttons      = require "buttons"
local colors       = require "colors"
local composer     = require "composer"
local fonts        = require "fonts"
local load_bar_lib = require "load_bar"
local ui           = require "ui"


--------------------------------------------
-- Script Constants
--------------------------------------------
-- Game title
local TITLE_Y         = 0.125 * display.contentHeight
local TITLE_MAX_WIDTH = 0.925 * display.contentWidth
local TITLE_FONTSIZE  = 0.0625 * display.contentHeight 

-- Back button
local BACK_BTN_DIM = 0.08 * display.contentHeight
local BACK_BTN_DIAMETER = 0.15 * display.contentHeight
local BACK_BTN_CENTER_Y = 0.88 * display.contentHeight

-- Load bar
local LOAD_BAR_Y_PERSONAL = 0.58 * display.contentHeight
local LOAD_BAR_Y_MASTER = LOAD_BAR_Y_PERSONAL + 0.085 * display.contentHeight
local LOAD_BAR_TIME = 1250

-- Loading message
local LOAD_TEXT_Y = 0.74 * display.contentHeight
local LOAD_TEXT_FONTSIZE = 0.04 * display.contentHeight
local LOAD_TEXT_MAX_WIDTH = 0.925 * display.contentWidth


--------------------------------------------
-- Script Variables
--------------------------------------------
local scene = composer.newScene()
local personalLoadBarObject = nil
local masterLoadBarObject = nil
local loadText = nil
local backButton = nil
local backButtonIsActive = false
local statValues = {}


--------------------------------------------
-- Button Events
--------------------------------------------
local function onBackBtnPress(event)
    if event.phase ~= "ended" then
        return
    end
    if not backButtonIsActive then
        return
    end
    composer.gotoScene("scenes.select_difficulty", "slideDown", 500)
    return true
end


--------------------------------------------
-- Helper functions
--------------------------------------------
local function activateBackButton()
    transition.to(backButton, {
        alpha = 1.0,
        time = 100
    })
    backButtonIsActive = true
    loadText.text = "Report complete."
end


local function syncMasterTeacher()
    load_bar_lib.setBarRatio(masterLoadBarObject, 0.0)
    loadText.text = "Syncing with Headmaster..."
    load_bar_lib.fillBar(masterLoadBarObject, LOAD_BAR_TIME, activateBackButton)
end


local function deactivateBackButton()
    backButton.alpha = 0.5
    backButtonIsActive = false
    loadText.text = "Personalizing your curriculum..."
end


--------------------------------------------
-- Create Scene
--------------------------------------------
function scene:create(event)    
    -- Called when the scene's view does not exist
    local sceneGroup = self.view

    -- Add background
    local background = bg_gen.getBackgroundDisplay()
    sceneGroup:insert(background)

    -- Title text
    local titleText = display.newText({
        text = "QUESTION COMPLETE!",
        x = display.contentCenterX,
        y = TITLE_Y,
        font = fonts.getFontBold(),
        fontSize = TITLE_FONTSIZE,
        align = "center"
    })
    titleText:setFillColor(1.0)
    ui.constrainTextSizeIfNecessary(titleText, TITLE_MAX_WIDTH)
    sceneGroup:insert(titleText)

    -- Big back button
    backButton = buttons.drawIconButton(buttons.BACK, BACK_BTN_DIAMETER)
    backButton.x = display.contentCenterX
    backButton.y = BACK_BTN_CENTER_Y
    sceneGroup:insert(backButton)
    backButton:addEventListener("touch", onBackBtnPress)

    --[[
    -- Small back button
    backButton = buttons.drawSquareButton(buttons.BACK, BACK_BTN_DIM)
    backButton.x = 0.5 * BACK_BTN_DIM
    backButton.y = 0.5 * BACK_BTN_DIM
    sceneGroup:insert(backButton)
    backButton:addEventListener("touch", onBackBtnPress)
    backButton.alpha = 0.35
    ]]

    -- Personal load bar
    local personalLoadBar, personalLoadBarObj = load_bar_lib.drawLoadBar(ui.LOAD_BAR_WIDTH, ui.LOAD_BAR_HEIGHT)
    personalLoadBarObject = personalLoadBarObj
    load_bar_lib.setBarRatio(personalLoadBarObject, 0.0)
    personalLoadBar.x = display.contentCenterX
    personalLoadBar.y = LOAD_BAR_Y_PERSONAL
    sceneGroup:insert(personalLoadBar)

    -- Master load bar
    local masterLoadBar, masterLoadBarObj = load_bar_lib.drawLoadBar(ui.LOAD_BAR_WIDTH, ui.LOAD_BAR_HEIGHT)
    masterLoadBarObject = masterLoadBarObj
    load_bar_lib.setBarRatio(masterLoadBarObject, 0.0)
    load_bar_lib.setBarColor(masterLoadBarObject, colors.GOLD)
    masterLoadBar.x = display.contentCenterX
    masterLoadBar.y = LOAD_BAR_Y_MASTER
    sceneGroup:insert(masterLoadBar)

    -- Load message
    loadText = display.newText({
        text = "Updating curriculum...",
        x = display.contentCenterX,
        y = LOAD_TEXT_Y,
        font = fonts.getFontBold(),
        fontSize = LOAD_TEXT_FONTSIZE,
        align = "center"
    })
    loadText:setFillColor(1.0)
    ui.constrainTextSizeIfNecessary(loadText, LOAD_TEXT_MAX_WIDTH)
    sceneGroup:insert(loadText)

    -- Level stats
    local stats = {
        "Difficulty:",
        "Num correct:",
        "Num incorrect:",
        --"Time taken:",
        "",
        "Predicted difficulty:",
        "Actual difficulty:"
    }
    for i = 1, #stats do 
        local y = 0.225 * display.contentHeight + (i - 1) * 0.05 * display.contentHeight

        local statText = display.newText({
            text = stats[i],
            font = fonts.getFont(),
            fontSize = 0.04 * display.contentHeight,
            x = 0.125 * display.contentWidth,
            y = y            
        })
        statText.anchorX = 0.0
        sceneGroup:insert(statText)

        local valueText = display.newText({
            text = "",
            font = fonts.getFontBold(),
            fontSize = 0.04 * display.contentHeight,
            x = 0.875 * display.contentWidth,
            y = y
        })
        valueText.anchorX = 1.0
        sceneGroup:insert(valueText)
        table.insert(statValues, valueText)
    end


    -- Set scene
    deactivateBackButton()
end


--------------------------------------------
-- Show Scene
--------------------------------------------
function scene:show(event)
    local sceneGroup = self.view
    local phase = event.phase
    
    if phase == "will" then
        -- Called when the scene is still off screen and is about to move on screen
        local params = event.params
        if params then
            if params.levelNumber then
                statValues[1].text = params.levelNumber
            end
            if params.numCorrect then
                statValues[2].text = params.numCorrect
            end
            if params.numIncorrect then
                statValues[3].text = params.numIncorrect
            end
            if params.predictedDifficulty then
                statValues[5].text = math.round(100 * params.predictedDifficulty) / 100.0
            end
            if params.actualDifficulty then
                statValues[6].text = math.round(100 * params.actualDifficulty) / 100.0
            end
        end
    elseif phase == "did" then
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc.
        load_bar_lib.fillBar(personalLoadBarObject, LOAD_BAR_TIME, syncMasterTeacher)
    end 
end


--------------------------------------------
-- Hide Scene
--------------------------------------------
function scene:hide(event)
    local sceneGroup = self.view
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
    elseif phase == "did" then
        -- Called when the scene is now off screen
        composer.removeScene("scenes.game_over")
    end 
end


--------------------------------------------
-- Destroy Scene
--------------------------------------------
function scene:destroy(event)
    local sceneGroup = self.view
    
    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
end


--------------------------------------------
-- Scene listener setup
--------------------------------------------
scene:addEventListener("create" , scene)
scene:addEventListener("show"   , scene)
scene:addEventListener("hide"   , scene)
scene:addEventListener("destroy", scene)
return scene