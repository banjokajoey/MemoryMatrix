-----------------------------------------------------------------------------------------
--
-- select_difficulty.lua
--
-----------------------------------------------------------------------------------------

--------------------------------------------
-- Imports
--------------------------------------------
local bg_gen       = require "background_generator"
local buttons      = require "buttons"
local colors       = require "colors"
local composer     = require "composer"
local fonts        = require "fonts"
local grid_lib     = require "grid"
local ui           = require "ui"


--------------------------------------------
-- Script Constants
--------------------------------------------
-- Levels
local GRID_DIM = 3

-- Game title
local TITLE_Y         = 0.125 * display.contentHeight
local TITLE_MAX_WIDTH = 0.925 * display.contentWidth
local TITLE_FONTSIZE  = 0.0625 * display.contentHeight 

-- Home button
local HOME_BTN_DIM = 0.08 * display.contentHeight

-- Play button
local PLAY_BTN_CENTER_Y = 0.9 * display.contentHeight
local PLAY_BTN_WIDTH = 0.8 * display.contentWidth
local PLAY_BTN_HEIGHT = 0.125 * display.contentHeight

-- Level select box
local LEVEL_BOX_DIM = 0.9 * display.contentWidth

-- Scene defaults
local DEFAULT_SELECTED_LEVEL = 1

--------------------------------------------
-- Script Variables
--------------------------------------------
local scene = composer.newScene()
local gridObject = nil
local selectedLevel = nil


--------------------------------------------
-- Helper functions
--------------------------------------------
local function selectCell(cellIndex)
    for i = 1, #gridObject.cells do
        local cell = gridObject.cells[i]
        if i == cellIndex then
            colors.colorObject(cell, colors.SKY_BLUE)
        else
            colors.colorObject(cell, colors.BLACK)
        end
    end
    selectedLevel = cellIndex
end


--------------------------------------------
-- Button Events
--------------------------------------------
local function onHomeBtnPress(event)
    if event.phase ~= "ended" then
        return
    end
    composer.gotoScene("scenes.home", {
        effect = "slideRight", 
        time = 500,
    })
    return true
end



local function onPlayBtnPress(event)
    if event.phase ~= "ended" then
        return
    end
    composer.gotoScene("scenes.play", {
        effect = "slideUp", 
        time = 500,
        params = {
            levelNumber = selectedLevel
        }
    })
    return true
end


local function onLevelCellPress(event, cellIndex)
    if event.phase ~= "ended" then
        return
    end
    selectCell(cellIndex)
    return true
end


--------------------------------------------
-- Create Scene
--------------------------------------------
function scene:create(event)    
    -- Called when the scene's view does not exist
    local sceneGroup = self.view

    -- Add background
    local background = bg_gen.getBackgroundDisplay()
    sceneGroup:insert(background)

    -- Home button
    homeButton = buttons.drawSquareButton(buttons.HOME, HOME_BTN_DIM)
    homeButton.x = 0.5 * HOME_BTN_DIM
    homeButton.y = 0.5 * HOME_BTN_DIM
    sceneGroup:insert(homeButton)
    homeButton:addEventListener("touch", onHomeBtnPress)
    homeButton.alpha = 0.35

    -- Title text
    local titleText = display.newText({
        text = "SELECT DIFFICULTY",
        x = display.contentCenterX,
        y = TITLE_Y,
        font = fonts.getFontBold(),
        fontSize = TITLE_FONTSIZE,
        align = "center"
    })
    titleText:setFillColor(1.0)
    ui.constrainTextSizeIfNecessary(titleText, TITLE_MAX_WIDTH)
    sceneGroup:insert(titleText)

    -- Levels select grid
    local grid, gridObj = grid_lib.drawGrid(GRID_DIM, LEVEL_BOX_DIM)
    gridObject = gridObj
    grid.x = display.contentCenterX
    grid.y = display.contentCenterY
    sceneGroup:insert(grid)
    grid_lib.numberCells(gridObject)
    for i = 1, #gridObject.cells do
        local cell = gridObject.cells[i]
        cell:addEventListener("touch", function(event) onLevelCellPress(event, i) end)
    end

    -- Play button
    local playBtn = buttons.drawWideButton(buttons.PLAY, PLAY_BTN_WIDTH, PLAY_BTN_HEIGHT)
    playBtn.x = display.contentCenterX
    playBtn.y = PLAY_BTN_CENTER_Y
    sceneGroup:insert(playBtn)
    playBtn:addEventListener("touch", onPlayBtnPress)

    -- Initialize scene
    selectCell(DEFAULT_SELECTED_LEVEL)
end


--------------------------------------------
-- Show Scene
--------------------------------------------
function scene:show(event)
    local sceneGroup = self.view
    local phase = event.phase
    
    if phase == "will" then
        -- Called when the scene is still off screen and is about to move on screen
    elseif phase == "did" then
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc.
    end 
end


--------------------------------------------
-- Hide Scene
--------------------------------------------
function scene:hide(event)
    local sceneGroup = self.view
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
    elseif phase == "did" then
        -- Called when the scene is now off screen
        composer.removeScene("scenes.select_difficulty")
    end 
end


--------------------------------------------
-- Destroy Scene
--------------------------------------------
function scene:destroy(event)
    local sceneGroup = self.view
    
    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
end


--------------------------------------------
-- Scene listener setup
--------------------------------------------
scene:addEventListener("create" , scene)
scene:addEventListener("show"   , scene)
scene:addEventListener("hide"   , scene)
scene:addEventListener("destroy", scene)
return scene