-----------------------------------------------------------------------------------------
--
-- splash.lua
--
-----------------------------------------------------------------------------------------

--------------------------------------------
-- Imports
--------------------------------------------
local bg_gen     = require "background_generator"
local colors     = require "colors"
local composer   = require "composer"
local easing     = require "easing"
local fonts      = require "fonts"
local ui         = require "ui"


--------------------------------------------
-- Script Variables
--------------------------------------------
-- Timing
local SPLASH_TIME = 2500

-- Text position
local TEAM_JOEY_Y = 0.45 * display.contentHeight
local HACK_FOR_NEURONS_Y = 0.515 * display.contentHeight

-- Text sizes
local TEAM_JOEY_FONTSIZE = 0.0775 * display.contentWidth
local HACK_FOR_NEURONS_FONTSIZE = 0.06 * display.contentWidth

-- Timing
local FADE_IN_DELAY    = 250
local TEXT_FADE_TIME   = 1250


--------------------------------------------
-- Debug Variables
--------------------------------------------
local debugHoldOnSplash = true


--------------------------------------------
-- Script Variables
--------------------------------------------
local scene = composer.newScene()
local textGroup = nil


--------------------------------------------
-- Button events
--------------------------------------------
local function onBackgroundPressed()
    composer.gotoScene("scenes.home", "slideLeft", 500)
    return true
end


--------------------------------------------
-- Helper functions
--------------------------------------------
local function playSplashSequence()
    -- Text fade in
    transition.to(textGroup, {
        alpha  = 1.0,
        time   = TEXT_FADE_TIME,
        delay  = FADE_IN_DELAY,
        easing = transition.inSine,
        onComplete = function()
            -- move to next scene
            if not debugHoldOnSplash then
                composer.gotoScene("scenes.home", "slideLeft", 500)
            end
        end
    })
end


--------------------------------------------
-- Create Scene
--------------------------------------------
function scene:create(event)    
    -- Called when the scene's view does not exist
    local sceneGroup = self.view

    -- Add background
    local background = bg_gen.getSplashBackgroundDisplay()
    sceneGroup:insert(background)
    background:addEventListener("touch", onBackgroundPressed)

    -- Splash group
    splashGroup = display.newGroup()
    sceneGroup:insert(splashGroup)

    -- Team text
    local teamJoeyText = display.newText({
        text = "TEAM JOEY",
        x = display.contentCenterX,
        y = TEAM_JOEY_Y,
        font = fonts.getFontBold(),
        fontSize = TEAM_JOEY_FONTSIZE,
        align = "center"
    })
    teamJoeyText:setFillColor(1.0)

    -- Hack for Neurons text
    local hackForNeuronsText = display.newText({
        text = "Hack for Neurons 9/9/2017",
        x = display.contentCenterX,
        y = HACK_FOR_NEURONS_Y,
        font = fonts.getFontBold(),
        fontSize = HACK_FOR_NEURONS_FONTSIZE,
        align = "center"
    })
    hackForNeuronsText:setFillColor(1.0)

    -- Responsive text sizes
    local maxTextWidth = math.min(400, 0.8 * display.contentWidth)
    ui.constrainTextSizeIfNecessary(teamJoeyText, maxTextWidth)
    ui.constrainTextSizeIfNecessary(hackForNeuronsText, maxTextWidth)
    
    -- Text group
    textGroup = display.newGroup()
    textGroup:insert(teamJoeyText)
    textGroup:insert(hackForNeuronsText)
    textGroup.alpha = 0.0
    sceneGroup:insert(textGroup)
end


--------------------------------------------
-- Show Scene
--------------------------------------------
function scene:show(event)
    local sceneGroup = self.view
    local phase = event.phase
    
    if phase == "will" then
        -- Called when the scene is still off screen and is about to move on screen
    elseif phase == "did" then
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc.
        playSplashSequence()
    end 
end


--------------------------------------------
-- Hide Scene
--------------------------------------------
function scene:hide( event )
    local sceneGroup = self.view
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
    elseif phase == "did" then
        -- Called when the scene is now off screen
        composer.removeScene("scenes.splash")
    end 
end


--------------------------------------------
-- Destroy Scene
--------------------------------------------
function scene:destroy( event )
    local sceneGroup = self.view
    
    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
end


--------------------------------------------
-- Scene listener setup
--------------------------------------------
scene:addEventListener("create" , scene)
scene:addEventListener("show"   , scene)
scene:addEventListener("hide"   , scene)
scene:addEventListener("destroy", scene)
return scene