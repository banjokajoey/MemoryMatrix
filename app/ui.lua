-----------------------------------------------------------------------------------------
--
-- ui.lua
--
-----------------------------------------------------------------------------------------

local ui = {}


--------------------------------------------
-- Public constants
--------------------------------------------
ui.X_PADDING  = 0.025  * display.contentWidth
ui.Y_PADDING  = 0.030  * display.contentWidth
ui.CORNER_RAD = 0.5 * ui.X_PADDING
ui.LOAD_BAR_WIDTH = 0.825 * display.contentWidth
ui.LOAD_BAR_HEIGHT = 0.02 * display.contentHeight


-------------------------------------
-- Public methods
-------------------------------------
ui.constrainTextSizeIfNecessary = function(textLabel, maxWidth)
    local scale = math.min(1.0, maxWidth / textLabel.width)
    textLabel.xScale = scale
    textLabel.yScale = scale
end


--------------------------------------------
-- Return
--------------------------------------------
return ui

