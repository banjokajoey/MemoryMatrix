-----------------------------------------------------------------------------------------
--
-- buttons.lua
--
-----------------------------------------------------------------------------------------

local buttons = {}


--------------------------------------------
-- Imports
--------------------------------------------
local colors     = require "colors"
local ui         = require "ui"



--------------------------------------------
-- Button enum and image mapping
--------------------------------------------
buttons.NONE = 0
buttons.PLAY = 1
buttons.BACK = 2
buttons.HOME = 3


local BTN_IMAGE_DIR_PATH = "images/icons/"
local BTN_IMAGE_NAMES = {}
BTN_IMAGE_NAMES[buttons.PLAY] = "play.png"
BTN_IMAGE_NAMES[buttons.BACK] = "back_arrow.png"
BTN_IMAGE_NAMES[buttons.HOME] = "home.png"


--------------------------------------------
-- Script constants
--------------------------------------------
local ICON_RATIO_TO_BG = 0.54
local RIM_THICKNESS_RATIO = 0.25


--------------------------------------------
-- Helper functions
--------------------------------------------
local function getButtonImage(btnType, btnDiameter)
    local img = nil
    if btnType == buttons.NONE then
        return img
    end
    
    local imgPath = BTN_IMAGE_DIR_PATH .. BTN_IMAGE_NAMES[btnType]
    local imgDim = ICON_RATIO_TO_BG * btnDiameter

    img = display.newImage(imgPath)
    img.width = imgDim
    img.height = imgDim
    return img
end


--------------------------------------------
-- Public methods
--------------------------------------------
buttons.drawIconButton = function(btnType, btnDiameter)
    -- Calculate radii
    local fullRadius = btnDiameter / 2
    local bubbleRadius = (1 - RIM_THICKNESS_RATIO) * fullRadius

    -- Bubble rim
    local bubbleRim = display.newCircle(0, 0, fullRadius)
    bubbleRim.anchorX = 0.5
    bubbleRim.anchorY = 0.5
    colors.colorObject(bubbleRim, colors.WHITE)

    -- Inner bubble
    local bubbleBg = display.newCircle(0, 0, bubbleRadius)
    bubbleBg.anchorX = 0.5
    bubbleBg.anchorY = 0.5
    colors.colorObject(bubbleBg, colors.BLACK)

    -- Button image
    local img = nil
    if btnType ~= buttons.NONE then
        img = getButtonImage(btnType, btnDiameter)
    end

    -- Button group
    local btnGrp = display.newGroup()
    btnGrp:insert(bubbleRim)
    btnGrp:insert(bubbleBg)
    btnGrp:insert(img)

    -- Return
    return btnGrp, bubbleRim, bubbleBg, img
end


buttons.drawSquareButton = function(btnType, btnDim)
    -- Background
    local background = display.newRect(0, 0, btnDim, btnDim)
    colors.colorObject(background, colors.OFF_BLACK)

    -- Button image
    local img = nil
    if btnType ~= buttons.NONE then
        img = getButtonImage(btnType, btnDim)
    end

    -- Button group
    local btnGrp = display.newGroup()
    btnGrp:insert(background)
    btnGrp:insert(img)

    -- Return
    return btnGrp, background, img
end


buttons.drawWideButton = function(btnType, btnWidth, btnHeight)
    -- Outer rect
    local outerCornerRadius = btnHeight / 2
    local outerRect = display.newRoundedRect(0, 0, btnWidth, btnHeight, outerCornerRadius)
    colors.colorObject(outerRect, colors.WHITE)

    -- Inner rect
    local thickness = 0.225 * btnHeight
    local innerHeight = btnHeight - thickness
    local innerWidth = btnWidth - thickness
    local innerCornerRadius = innerHeight / 2
    local innerRect = display.newRoundedRect(0, 0, innerWidth, innerHeight, innerCornerRadius)
    colors.colorObject(innerRect, colors.BLACK)

    -- Button icon
    local icon = getButtonImage(btnType, btnHeight)

    -- Assemble final group
    local finalGrp = display.newGroup()
    finalGrp:insert(outerRect)
    finalGrp:insert(innerRect)
    if icon then
        finalGrp:insert(icon)
    end
    return finalGrp, outerRect, innerRect, icon
end


--------------------------------------------
-- Return
--------------------------------------------
return buttons