-----------------------------------------------------------------------------------------
--
-- level.lua
--
-----------------------------------------------------------------------------------------

local level = {}


--------------------------------------------
-- Public methods
--------------------------------------------
level.addRandomCell = function(levelObject)
    local attemptsLeft = 300
    while attemptsLeft > 0 do
        local cellIndex = math.random(#(levelObject.grid))
        if not levelObject.grid[cellIndex] then
            levelObject.grid[cellIndex] = true
            --levelObject.numValues = levelObject.numValues + 1
            local numValues = 0
            for i = 1, #levelObject.grid do
                if levelObject.grid[i] then
                    numValues = numValues + 1
                end
            end
            levelObject.numValues = numValues
            break
        end
        attemptsLeft = attemptsLeft - 1
    end
end


level.loadBlank = function()
    local gridValues = {}
    for i = 1, 36 do
        table.insert(gridValues, false)
    end

    local levelObject = {
        grid = gridValues,
        numValues = numValues,
        numCorrect = 0,
        numIncorrect = 0
    }

    return levelObject
end


level.loadSmall = function(levelObject)
    local levelObject = level.loadBlank()
    levelObject.grid[1] = true

    local numValues = 0
    for i = 1, #levelObject.grid do
        if levelObject.grid[i] then
            numValues = numValues + 1
        end
    end
    levelObject.numValues = numValues

    return levelObject
end


level.getLevel = function(difficulty)
    local blankLevel = level.loadBlank()
    for i = 1, (2 * difficulty) do
        level.addRandomCell(blankLevel)
    end
    return blankLevel
end


--------------------------------------------
-- Return
--------------------------------------------
return level