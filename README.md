# [Hack for Neurons](https://www.hackforneurons.com/) (September 9, 2017)

## Overview

_MEMORY MATRIX_ is a brain-training game lifted straight from the [Lumosity](http://www.lumosity.com) catalog. It is a game designed to be increasingly demanding of the player's spatial memory, with hopes that repeated practice could result in longterm cognitive benefits.

This repo's implementation takes a modified approach to the Lumosity game. Instead of serving up predefined, static levels, this version procedurally generates levels based on a small set of parameters. By generating levels from a numeric template, we enable the opportunity to create levels using Machine Learning. Such a setup has enormous potential for the precise tuning of game levels.

This particular implementation uses Machine Learning to predict how difficult a level will be based only on its template parameters. This predictor is used to produce levels that are appropriately difficult for the player relative to their training curriculum. With a well trained predictor, this methodology could provide unprecedented precision when tuning difficulty curves, an important task in brain-training games that has historically been done by hand.


## Distributed Edge Learning

In the spirit of the _Hack for Neurons_ hackathon, this application simulates a framework for distributed edge learning using the paradigm of a school's "Headmaster" distributing a "personalized curriculum".

What this amounts to is two machine learning models, one on the player's phone and one in the cloud. When the individual begins playing, this app synchronizes the two machine learning models. Then, as the game is played, the player's personal game data is used to train the model on their phone. Periodically, this personal model is synced with the cloud, which  integrates the player's information into the master model. With multiple players involved in this process, the cloud model becomes a good predictor of level difficulty for the playing population as a whole, while each individual's phone becomes a good personalized predictor.

![System Diagram](diagrams/memory_matrix_system_diagram_171009.png)


## Inspiration

Brain training games have had a popularity surge in the past decade as more individuals have had the desire to play an active role in their mental health. Brain games are noninvasivem, measurable, and entertaining, and many private companies have spun up in order to support this demand.

However, despite their recent popularity, such games frequently receive negative feedback from the scientific community. The data surrounding game-based brain training yields murky results, with some evidence supporting the benefits and with other evidence casting doubt upon longterm results.

This project was an effort to combat this limbo by adding scientific rigor to the game design process. If brain-game designers have access to tools that allow them to accurately and precisely tune their game's mechanics, then perhaps their games could yield stronger, more statistically verifiable results. Or, if they fail to do so, perhaps these designers could move on to other pursuits.

Either way, we will not be able to tell for certain whether brain games are medically viable treatments until we have greater control over the games we make.


## Level-tuning Parameters

Parameters currently taken into account are:

* number of cells to remember

Extremely useful parameters to implement in the future could include:

* number of chunks to remember
* P (chunkSize == 1)
* P (chunkSize == 2)
* P (chunkSize == 3)
* P (chunkSize == 5)
* P (chunkSize == 4)
* P (chunkSize == 6)
* number of milliseconds the user is alloted for memorization


## Team

* Joseph Hernandez (josephhernandez5@gmail.com)


## Screenshots

![Home](screenshots/home.png)
![Select Difficulty](screenshots/select_difficulty.png)
![Play](screenshots/play.png)
![Game Over](screenshots/game_over.png)